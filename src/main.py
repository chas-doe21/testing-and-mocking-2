from fastapi import FastAPI
from fastapi.testclient import TestClient
from unittest.mock import MagicMock, Mock
import psycopg

app = FastAPI()


@app.on_event('startup')
def setup_db():
    app.db = psycopg.connect("postgresql://postgres:testpass@localhost/mocking")


@app.on_event("shutdown")
def shutdown_db():
    app.db.close()


@app.get("/")
def root():
    with app.db.cursor() as cur:
        cur.execute("SELECT * FROM DATA")
        data = cur.fetchall()
        return data


class CursorMock(MagicMock):
    fetchall = Mock(return_value=[[1, 'a', 'b']])


class DBMock:
    cursor = CursorMock


def test_root():
    client = TestClient(app)
    # setup_db()
    app.db = DBMock()
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == [[1, "a", "b"]]
    # shutdown_db
